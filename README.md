# GodotVibrate

**IMPORTANT**: This was meant to be used with Godot 2.1.x. It probably works with 3.0+, but you
should do it with [GDNative](https://godot.readthedocs.io/en/3.0/tutorials/plugins/gdnative/index.html) instead.

Module for [Godot Engine](https://godotengine.org) that enables vibration. It works only on Android.

## How to install

Move the "vibrate" folder to the "modules" folder inside Godot Engine source code.
Then, recompile to obtain the android export templates ([follow the official documentation](https://godot.readthedocs.io/en/stable/development/compiling/compiling_for_android.html))

To export using the module, go to Export -> Target -> Android. Then, place in the "Custom package" field your debug and release apk that were generated before. Then, scroll down and activate the "Vibrate" permission.

After that, add the module to the [android] section of your project's engine.cfg file:

    [android]
    modules="/org/godotengine/godot/GodotVibrate"

## How to use

First, create a variable that stores the singleton:

    var vibrator = Globals.get_singleton("GodotVibrate")

Then, use it by calling the "vibrate" method. Its only argument is the duration of the vibration, in milliseconds.

    vibrator.vibrate(500) # vibrate for 500 milliseconds
