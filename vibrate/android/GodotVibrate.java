package org.godotengine.godot;

import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.os.Vibrator;

public class GodotVibrate extends Godot.SingletonBase {
	private Activity activity = null; 

	public void vibrate(final int ms) {
		activity.runOnUiThread(new Runnable() {
			@Override public void run() {
				Vibrator vib = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
				vib.vibrate(ms);
			}
		});
	}
	
 	static public Godot.SingletonBase initialize(Activity activity) {
 		return new GodotVibrate(activity);
 	}

	public GodotVibrate(Activity activity) {
		registerClass("GodotVibrate", new String[] {"vibrate"});
		this.activity = activity;
	}
}
